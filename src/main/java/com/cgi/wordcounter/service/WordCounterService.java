package com.cgi.wordcounter.service;

import java.util.Map;

public interface WordCounterService {
    Map<String, Integer> countWords(String wordsInput);
}
