package com.cgi.wordcounter.service;

import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class WordCounterServiceImpl implements WordCounterService {

    @Override
    // Implements the countWords method in the interface
    public Map<String, Integer> countWords(String inputString) {
        // Split the input string into an array of words using space as a delimiter
        String[] words = inputString.split("\\s+");
        // Initializes an empty map to store the word count
        Map<String, Integer> wordCountMap = new HashMap<>();
        // Uses a stream to iterate over each word in the words array and count them
        Arrays.stream(words).forEach(word -> {
            Integer count = wordCountMap.get(word);
            if (count == null) {
                wordCountMap.put(word, 1);
            } else {
                wordCountMap.put(word, count + 1);
            }
        });
        // create a list so we can do some sorting job.
        List<Map.Entry<String, Integer>> entryList = new ArrayList<>(wordCountMap.entrySet());
        // Sort the values from high to low in the list.
        Collections.sort(entryList, Map.Entry.comparingByValue(Comparator.reverseOrder()));
        // Create a LinkedHashMap and store the sorted data from above.
        Map<String, Integer> sortedWordCountMap = new LinkedHashMap<>();
        // Add the sorted entry to the new map.
        for (Map.Entry<String, Integer> entry : entryList) {
            sortedWordCountMap.put(entry.getKey(), entry.getValue());
        }
        // Returns the sorted map
        return sortedWordCountMap;
    }
}
