package com.cgi.wordcounter.controller;

import com.cgi.wordcounter.service.WordCounterService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class WordCounterController {

    private final WordCounterService wordCounterService;

    // The instance of WordCounterService is injected using the constructor.
    public WordCounterController(WordCounterService wordCounterService) {
        this.wordCounterService = wordCounterService;
    }

    @PostMapping("/count")
    /* This endpoint listens for a POST request and expects the request body to
    include the input string.
     */
    public ResponseEntity<Map<String, Integer>> countWords(@RequestBody String inputString) {
        // Check if the input is null or empty, if it is true then return bad request.
        if (inputString == null || inputString.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        // Invoke the countWords of the WordCounterService to get a map of word counts
        Map<String, Integer> wordCountMap = wordCounterService.countWords(inputString);

        /* This code returns a HTTP response with a status code of 200 (OK)
        and includes the word count map in the response body.
         */
        return ResponseEntity.ok(wordCountMap);
    }
}
