# Cgi-WordCounter



## Short about this application

This word counter is a simple example of how to create a Java Spring Boot application with a RESTful API endpoint that counts every word entered by users.
We split the words by checking the SPACE between each words.

## Clone this repo and open up with your IDE.
**Is this project I used the following.**

Java<br>
Spring Boot<br>
Maven<br>
Intellij<br>

## Using the API
The Word Counter API accepts a POST request to the /count endpoint with a request body containing the input string from user.

The API returns a JSON or Plain Text containing the count of each word from the user input.<br><br>
**EXAMPLE:<br>**
https://imgur.com/a/x476Vxw

You can also try<br> 
$> curl -H "Content-type: text/plain" -X "POST" -d "Banan Äpple Katt Hund Banan
Hund Katt Hund" http://localhost:3000/count

The retun should look like this:<br>
$> {"Hund":3,"Banan":2,"Katt":2,"Äpple":1}

##  Integration with the application
To integrate the Word Counter API with your application, simply send a POST request to the /count endpoint with the input string as the request body.

##  Good to know.
This application uses server port 3000 by default, you can change this in application.properties.<br>server.port=3000
